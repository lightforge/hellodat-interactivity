function getFormData(el) {
  return [...el.querySelectorAll(`[name]`)].reduce((o, el) => {
    if (el.name) {
      o[el.name] = el.value;
    }
    return o
  }, {})
}

const inputsMap = {
  'text': ({id, name, value}) => `<input required id="${id}" name="${name}"" class="form-control" value="${value}" />`,
};

const normalizersMap = {
  'array': v => v==='' ? [] : v.replace(/[\[\] \n`'"]/g, '').split(','),
  'arrayNumber': v => v==='' ? [] : v.replace(/[\[\] \n`'"]/g, '').split(',').map(v => Number(v)),
  'number': v => Number(v),
}

let inputCounter = 0;
const input = (props) => {
  let {name, value, type, label} = props;
  const id = `${name}-${inputCounter++}`;
  return {
    value: () => {
      const v = document.querySelector('#' + id).value;
      return normalizersMap[type] ? normalizersMap[type](v) : v
    },
    toString: ({values}) => `<div class="form-group">
            <label for="${id}">${label || name} [${type}]</label>
            ${(inputsMap[type] || inputsMap['text'])({id, name, value: values[name] || ''})}
        </div>`,
    ...props,
  };
};

async function getParams({el, namespace, title, formName, inputs, initialValues}) {
  if ([el, inputs, namespace, formName, initialValues].includes(undefined)) throw new Error('missing parameter');
  let fileData = {};
  let namespaceData = {};
  let formData = {};
  try {
    fileData = await iodide.file.load('params.json', 'json');
    // console.log('fileData', fileData)
    namespaceData = fileData[namespace] || {};
    formData = namespaceData[formName] || initialValues;
    // console.log('formData', formData)
  } catch (e) {
    formData = initialValues;
  }
  el.innerHTML = `
  <form class="container my-5">
    <h2>${title || formName}</h2>
    ${inputs.map(input => input.toString({values: formData})).join('')}
    <div class="form-actions">
      <button type="submit" class="btn btn-primary">Continue</button>
    </div>
  </form>
  `;
  return new Promise((resolve, reject) => {
    el.querySelector('.btn-primary').addEventListener('click', async (e) => {
      e.preventDefault();
      const formData = window[formName] = getFormData(el);
      fileData = {...fileData, [namespace]: {...namespaceData, [formName]: formData}};
      await iodide.file.save('params.json', 'json', fileData, {overwrite: true});
      for (let input of inputs) {
        formData[input.name] = input.value()
      }
      el.querySelector('.btn-primary').classList.add('disabled');
      resolve(formData);
      return false;
    }, {once: true})
  })
}